AstroSolver
===========

Little helper tool to use some of the basic equations from BMW to solve small
problems.

If you want the native backend to run in Linux do the equivalent of:

```
$ sudo apt-get install libxcb-render0-dev libxcb-shape0-dev libxcb-xfixes0-dev libspeechd-dev libxkbcommon-dev libssl-dev
```

for your distribution.
