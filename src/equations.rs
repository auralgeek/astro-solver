#![allow(non_snake_case)]
use crate::system::*;
use crate::*;
use toml::Value;

use std::error::Error;
use std::fs;

// TODO: Make these swappable with Heliocentric or Lunar or other planets
// Geocentric
pub struct Constants {
    pub mu: f64,
    pub DU: f64,
    pub TU: f64,
    pub DUTU: f64,
    pub omega: f64,
}

static Geocentric: Constants = Constants {
    mu: 3.986012e5,       // Gravitational parameter (km^3/s^2)
    DU: 6378.136,         // Earth radius (km)
    TU: 806.8118744,      // Time unit (solar sec.)
    DUTU: 7.90536828,     // Speed unit (km/s) DU/TU
    omega: 7.29211586e-5, // Angular rotation (rad/s)
};

// Start with my favorite equation: E = |v|^2 / 2 - mu / |r|
pub fn specific_mechanical_energy1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let r = variables.get("r").expect(msg);
    let v = variables.get("v").expect(msg);
    if r.abs() < 1e-6 {
        None
    } else {
        Some(v.abs().powi(2) / 2. - Geocentric.mu / r.abs())
    }
}

// r = mu / ( |v|^2 / 2 - E )
pub fn specific_mechanical_energy2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let E = variables.get("E").expect(msg);
    let v = variables.get("v").expect(msg);
    if (v.abs().powi(2) / 2. - E).abs() < 1e-6 {
        None
    } else {
        Some(Geocentric.mu / (v.abs().powi(2) / 2. - E))
    }
}

// v = sqrt { 2 * ( E + mu / r ) }
pub fn specific_mechanical_energy3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let E = variables.get("E").expect(msg);
    let r = variables.get("r").expect(msg);
    if r.abs() < 1e-6 || E + Geocentric.mu / r < 0.0 {
        None
    } else {
        Some((2. * (E + Geocentric.mu / r)).sqrt())
    }
}

// h = r x v
pub fn angular_momentum(variables: &Variables) -> [f64; 3] {
    // TODO: Have to figure out how to handle returns of type [f64; 3]
    todo!()
}

// Flight path angle
// h = |r| |v| cos(phi)
pub fn flight_path_angle1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let r = variables.get("r").expect(msg);
    let v = variables.get("v").expect(msg);
    let phi = variables.get("phi").expect(msg);
    Some(r * v * phi.cos())
}

// r = h / ( |v| * cos(phi) )
pub fn flight_path_angle2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let v = variables.get("v").expect(msg);
    let phi = variables.get("phi").expect(msg);
    if (v * phi.cos()).abs() < 1e-6 {
        None
    } else {
        Some(h / (v * phi.cos()))
    }
}

// v = h / ( |r| * cos(phi) )
pub fn flight_path_angle3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let r = variables.get("r").expect(msg);
    let phi = variables.get("phi").expect(msg);
    if (r * phi.cos()).abs() < 1e-6 {
        None
    } else {
        Some(h / (r * phi.cos()))
    }
}

// phi = arccos( h / ( |r| |v| ) )
pub fn flight_path_angle4(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let r = variables.get("r").expect(msg);
    let v = variables.get("v").expect(msg);
    if (r * v).abs() < 1e-6 {
        None
    } else {
        Some((h / (r * v)).acos())
    }
}

// r = p / ( 1 + e * cos(nu) )
pub fn polar_eq_of_conic1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let e = variables.get("e").expect(msg);
    let nu = variables.get("nu").expect(msg);
    if (1. + e * nu.cos()).abs() < 1e-6 {
        None
    } else {
        Some(p / (1. + e * nu.cos()))
    }
}

// p = r * ( 1 + e * cos(nu) )
pub fn polar_eq_of_conic2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let r = variables.get("r").expect(msg);
    let e = variables.get("e").expect(msg);
    let nu = variables.get("nu").expect(msg);
    Some(r * (1. + e * nu.cos()))
}

// e = ( p - r ) / cos(nu)
pub fn polar_eq_of_conic3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let r = variables.get("r").expect(msg);
    let nu = variables.get("nu").expect(msg);
    if nu.cos().abs() < 1e-6 {
        None
    } else {
        Some((p - r) / nu.cos())
    }
}

// nu = arccos{ ( p - r ) / e }
pub fn polar_eq_of_conic4(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let r = variables.get("r").expect(msg);
    let e = variables.get("e").expect(msg);
    if e.abs() < 1e-6 {
        None
    } else {
        Some(((p - r) / e).acos())
    }
}

// p = a ( 1 - e^2 )
pub fn p_a_e1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 1e-6 || a < 1e-6 {
        None
    } else {
        Some(a * (1. - e.powi(2)))
    }
}

// a = p / ( 1 - e^2 )
pub fn p_a_e2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 1e-6 || p < 1e-6 {
        None
    } else {
        Some(p / (1. - e.powi(2)))
    }
}

// e = sqrt { 1 - p / a }
pub fn p_a_e3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let a = variables.get("a").expect(msg);
    if a < 1e-6 || p < 1e-6 || 1. - p / a < 0.0 {
        None
    } else {
        Some((1. - p / a).sqrt())
    }
}

// rp = p / ( 1 + e )
pub fn rp_p_e1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 0.0 || p < 0.0 {
        None
    } else {
        Some(p / (1. + e))
    }
}

// p = rp * ( 1 + e )
pub fn rp_p_e2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let rp = variables.get("rp").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 0.0 || rp < 0.0 {
        None
    } else {
        Some(rp * (1. + e))
    }
}

// e = (p - rp ) / rp
pub fn rp_p_e3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let rp = variables.get("rp").expect(msg);
    if p < rp || rp < 1e-6 {
        None
    } else {
        Some((p - rp) / rp)
    }
}

// rp = a ( 1 - e )
pub fn rp_a_e1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let e = variables.get("e").expect(msg);
    if a < 0.0 || e < 0.0 {
        None
    } else {
        Some(a * (1. - e))
    }
}

// a = rp / ( 1 - e )
pub fn rp_a_e2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let rp = variables.get("rp").expect(msg);
    let e = variables.get("e").expect(msg);
    if rp < 0.0 || (1. - e).abs() < 1e-6 {
        None
    } else {
        Some(rp / (1. - e))
    }
}

// e = ( a - rp ) / a
pub fn rp_a_e3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let rp = variables.get("rp").expect(msg);
    if a < 1e-6 || a < rp {
        None
    } else {
        Some((a - rp) / a)
    }
}

// ra = p / ( 1 - e )
pub fn ra_p_e1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    let e = variables.get("e").expect(msg);
    if (1. - e).abs() < 1e-6 || p < 0.0 {
        None
    } else {
        Some(p / (1. - e))
    }
}

// p = ra * ( 1 - e )
pub fn ra_p_e2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 0.0 || ra < 0.0 {
        None
    } else {
        Some(ra * (1. - e))
    }
}

// e = ( ra - p ) / ra
pub fn ra_p_e3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let p = variables.get("p").expect(msg);
    if ra < p || ra < 1e-6 {
        None
    } else {
        Some((ra - p) / ra)
    }
}

// ra = a ( 1 + e )
pub fn ra_a_e1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let e = variables.get("e").expect(msg);
    if a < 0.0 || e < 0.0 {
        None
    } else {
        Some(a * (1. - e))
    }
}

// a = ra / ( 1 + e )
pub fn ra_a_e2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let e = variables.get("e").expect(msg);
    if ra < 0.0 || e < 0.0 {
        None
    } else {
        Some(ra / (1. + e))
    }
}

// e = ( ra - a ) / a
pub fn ra_a_e3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let ra = variables.get("ra").expect(msg);
    if ra < a || a.abs() < 1e-6 {
        None
    } else {
        Some((ra - a) / a)
    }
}

// p = h^2 / mu
pub fn p_h1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    Some(h.powi(2) / Geocentric.mu)
}

// h = sqrt( mu * p )
pub fn p_h2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let p = variables.get("p").expect(msg);
    if p < 0.0 {
        None
    } else {
        Some((Geocentric.mu * p).sqrt())
    }
}

// h = rp * vp
pub fn h_rp_vp1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let rp = variables.get("rp").expect(msg);
    let vp = variables.get("vp").expect(msg);
    if rp < 0.0 || vp < 0.0 {
        None
    } else {
        Some(rp * vp)
    }
}

// rp = h / vp
pub fn h_rp_vp2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let vp = variables.get("vp").expect(msg);
    if h < 0.0 || vp < 1e-6 {
        None
    } else {
        Some(h / vp)
    }
}

// vp = h / rp
pub fn h_rp_vp3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let rp = variables.get("rp").expect(msg);
    if h < 0.0 || rp < 1e-6 {
        None
    } else {
        Some(h / rp)
    }
}

// h = ra * va
pub fn h_ra_va1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let va = variables.get("va").expect(msg);
    if ra < 0.0 || va < 0.0 {
        None
    } else {
        Some(ra * va)
    }
}

// ra = h / va
pub fn h_ra_va2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let va = variables.get("va").expect(msg);
    if h < 0.0 || va < 1e-6 {
        None
    } else {
        Some(h / va)
    }
}

// va = h / ra
pub fn h_ra_va3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let h = variables.get("h").expect(msg);
    let ra = variables.get("ra").expect(msg);
    if h < 0.0 || ra < 1e-6 {
        None
    } else {
        Some(h / ra)
    }
}

// E = -mu / ( 2 * a )
pub fn E_a1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    if a < 1e-6 {
        None
    } else {
        Some(-Geocentric.mu / (2. * a))
    }
}

// a = -mu / (2 * E)
pub fn E_a2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let E = variables.get("E").expect(msg);
    if E > -1e-6 {
        None
    } else {
        Some(-Geocentric.mu / (2. * E))
    }
}

// e = sqrt( 1 + 2 * E * h^2 / mu^2 )
pub fn e_E_h1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let E = variables.get("E").expect(msg);
    let h = variables.get("h").expect(msg);
    if (1. + 2. * E * (h.powi(2) / Geocentric.mu.powi(2))) < 0.0 {
        None
    } else {
        Some((1. + 2. * E * (h.powi(2) / Geocentric.mu.powi(2))).sqrt())
    }
}

// E = mu^2 * ( e^2 - 1 ) / ( 2 * h^2 )
pub fn e_E_h2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let e = variables.get("e").expect(msg);
    let h = variables.get("h").expect(msg);
    if (2. * h.powi(2)).abs() < 1e-6 {
        None
    } else {
        Some(Geocentric.mu.powi(2) * (e.powi(2) - 1.) / (2. * h.powi(2)))
    }
}

// h = sqrt( mu^2 * ( e^2 - 1 ) / ( 2 * E ) )
pub fn e_E_h3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let e = variables.get("e").expect(msg);
    let E = variables.get("E").expect(msg);
    if (2. * E).abs() < 1e-6 || Geocentric.mu.powi(2) * (e.powi(2) - 1.) / (2. * E.powi(2)) < 0.0 {
        None
    } else {
        Some((Geocentric.mu.powi(2) * (e.powi(2) - 1.) / (2. * E.powi(2))).sqrt())
    }
}

// rp = 2 * a - ra
pub fn rp_a_ra1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let ra = variables.get("ra").expect(msg);
    if 2. * a < ra {
        None
    } else {
        Some(2. * a - ra)
    }
}

// a = ( rp + ra ) / 2
pub fn rp_a_ra2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let rp = variables.get("rp").expect(msg);
    let ra = variables.get("ra").expect(msg);
    if rp + ra < 0.0 {
        None
    } else {
        Some((rp + ra) / 2.)
    }
}

// ra = 2 * a - rp
pub fn rp_a_ra3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    let rp = variables.get("rp").expect(msg);
    if 2. * a < rp {
        None
    } else {
        Some(2. * a - rp)
    }
}

// e = ( ra - rp ) / ( ra + rp )
pub fn e_ra_rp1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let rp = variables.get("rp").expect(msg);
    if (ra + rp).abs() < 1e-6 || ra < rp {
        None
    } else {
        Some((ra - rp) / (ra + rp))
    }
}

// ra = rp * ( 1 + e ) / ( 1 - e )
pub fn e_ra_rp2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let rp = variables.get("rp").expect(msg);
    let e = variables.get("e").expect(msg);
    if (1. - e).abs() < 1e-6 || rp < 0.0 {
        None
    } else {
        Some(rp * (1. + e) / (1. - e))
    }
}

// rp = ra * ( 1 - e ) / ( 1 + e )
pub fn e_ra_rp3(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let ra = variables.get("ra").expect(msg);
    let e = variables.get("e").expect(msg);
    if e < 0.0 || ra < 0.0 {
        None
    } else {
        Some(ra * (1. - e) / (1. + e))
    }
}

// T = ( 2 * PI / sqrt( mu ) ) * sqrt(a^3)
pub fn T_a1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let a = variables.get("a").expect(msg);
    if a.powi(3) < 0.0 {
        None
    } else {
        Some((2. * std::f64::consts::PI / Geocentric.mu.sqrt()) * a.powi(3).sqrt())
    }
}

// a = cuberoot{ ( T * (sqrt( mu ) / ( 2 * PI ) ) )^2 }
pub fn T_a2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let T = variables.get("T").expect(msg);
    if T < 0.0 {
        None
    } else {
        Some(((T * Geocentric.mu.sqrt() / (2. * std::f64::consts::PI)).powi(2)).cbrt())
    }
}

// vesc = sqrt( 2 * mu / r )
pub fn vesc_r1(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let r = variables.get("r").expect(msg);
    if r < 1e-6 {
        None
    } else {
        Some((2. * Geocentric.mu / r).sqrt())
    }
}

// r = ( 2 * mu ) / vesc^2
pub fn vesc_r2(variables: &Variables) -> Option<f64> {
    let msg = "Can't solve without the parameter values defined";
    let vesc = variables.get("vesc").expect(msg);
    if vesc.powi(2) < 1e-6 {
        None
    } else {
        Some(2. * Geocentric.mu / vesc.powi(2))
    }
}

pub struct KeplerianEquations {
    pub variables: Variables,
    pub equations: Vec<Equation>,
}

impl KeplerianEquations {
    pub fn new() -> KeplerianEquations {
        let mut equations = vec![];
        let mut variables = Variables::new();

        // Set the symbolic variables up
        variables.set("E", None); // Specific mechanical energy
        variables.set("r", None); // Range from inertial origin
        variables.set("v", None); // Speed
        variables.set("h", None); // Angular momentum
        variables.set("phi", None); // Flight path angle (rad)
        variables.set("p", None); // Semi latus rectum
        variables.set("e", None); // Eccentricity
        variables.set("nu", None); // True anomaly

        // NOTE: a implicitly requires closed orbit
        variables.set("a", None); // Semimajor axis
        variables.set("rp", None); // Periapsis range

        // NOTE: ra implicitly requires closed orbit
        variables.set("ra", None); // Apoapsis range
        variables.set("vp", None); // Periapsis speed

        // NOTE: va implicitly requires closed orbit
        variables.set("va", None); // Apoapsis speed

        // Push in the equations
        equations.push(Equation::new(
            "E",
            &vec!["v", "r"],
            Box::new(specific_mechanical_energy1),
        ));
        equations.push(Equation::new(
            "r",
            &vec!["E", "v"],
            Box::new(specific_mechanical_energy2),
        ));
        equations.push(Equation::new(
            "v",
            &vec!["E", "r"],
            Box::new(specific_mechanical_energy3),
        ));
        equations.push(Equation::new(
            "h",
            &vec!["r", "v", "phi"],
            Box::new(flight_path_angle1),
        ));
        equations.push(Equation::new(
            "r",
            &vec!["h", "v", "phi"],
            Box::new(flight_path_angle2),
        ));
        equations.push(Equation::new(
            "v",
            &vec!["h", "r", "phi"],
            Box::new(flight_path_angle3),
        ));
        equations.push(Equation::new(
            "phi",
            &vec!["r", "v", "h"],
            Box::new(flight_path_angle4),
        ));
        equations.push(Equation::new(
            "r",
            &vec!["p", "e", "nu"],
            Box::new(polar_eq_of_conic1),
        ));
        equations.push(Equation::new(
            "p",
            &vec!["r", "e", "nu"],
            Box::new(polar_eq_of_conic2),
        ));
        equations.push(Equation::new(
            "e",
            &vec!["r", "p", "nu"],
            Box::new(polar_eq_of_conic3),
        ));
        equations.push(Equation::new(
            "nu",
            &vec!["r", "p", "e"],
            Box::new(polar_eq_of_conic4),
        ));
        equations.push(Equation::new("p", &vec!["a", "e"], Box::new(p_a_e1)));
        equations.push(Equation::new("a", &vec!["p", "e"], Box::new(p_a_e2)));
        equations.push(Equation::new("e", &vec!["p", "a"], Box::new(p_a_e3)));

        equations.push(Equation::new("rp", &vec!["p", "e"], Box::new(rp_p_e1)));
        equations.push(Equation::new("p", &vec!["rp", "e"], Box::new(rp_p_e2)));
        equations.push(Equation::new("e", &vec!["rp", "p"], Box::new(rp_p_e3)));
        equations.push(Equation::new("rp", &vec!["a", "e"], Box::new(rp_a_e1)));
        equations.push(Equation::new("a", &vec!["rp", "e"], Box::new(rp_a_e2)));
        equations.push(Equation::new("e", &vec!["rp", "a"], Box::new(rp_a_e3)));

        equations.push(Equation::new("ra", &vec!["p", "e"], Box::new(ra_p_e1)));
        equations.push(Equation::new("p", &vec!["ra", "e"], Box::new(ra_p_e2)));
        equations.push(Equation::new("e", &vec!["ra", "p"], Box::new(ra_p_e3)));
        equations.push(Equation::new("ra", &vec!["a", "e"], Box::new(ra_a_e1)));
        equations.push(Equation::new("a", &vec!["ra", "e"], Box::new(ra_a_e2)));
        equations.push(Equation::new("e", &vec!["ra", "a"], Box::new(ra_a_e3)));
        equations.push(Equation::new("p", &vec!["h"], Box::new(p_h1)));
        equations.push(Equation::new("h", &vec!["p"], Box::new(p_h2)));

        equations.push(Equation::new("h", &vec!["rp", "vp"], Box::new(h_rp_vp1)));
        equations.push(Equation::new("rp", &vec!["h", "vp"], Box::new(h_rp_vp2)));
        equations.push(Equation::new("vp", &vec!["h", "rp"], Box::new(h_rp_vp3)));
        equations.push(Equation::new("h", &vec!["ra", "va"], Box::new(h_ra_va1)));
        equations.push(Equation::new("ra", &vec!["h", "va"], Box::new(h_ra_va2)));
        equations.push(Equation::new("va", &vec!["h", "ra"], Box::new(h_ra_va3)));

        equations.push(Equation::new("E", &vec!["a"], Box::new(E_a1)));
        equations.push(Equation::new("a", &vec!["E"], Box::new(E_a2)));
        equations.push(Equation::new("e", &vec!["E", "h"], Box::new(e_E_h1)));
        equations.push(Equation::new("E", &vec!["e", "h"], Box::new(e_E_h2)));
        equations.push(Equation::new("h", &vec!["e", "E"], Box::new(e_E_h3)));
        equations.push(Equation::new("rp", &vec!["a", "ra"], Box::new(rp_a_ra1)));
        equations.push(Equation::new("a", &vec!["rp", "ra"], Box::new(rp_a_ra2)));
        equations.push(Equation::new("ra", &vec!["rp", "a"], Box::new(rp_a_ra3)));

        equations.push(Equation::new("e", &vec!["ra", "rp"], Box::new(e_ra_rp1)));
        equations.push(Equation::new("ra", &vec!["e", "rp"], Box::new(e_ra_rp2)));
        equations.push(Equation::new("rp", &vec!["e", "ra"], Box::new(e_ra_rp3)));
        equations.push(Equation::new("T", &vec!["a"], Box::new(T_a1)));
        equations.push(Equation::new("a", &vec!["T"], Box::new(T_a2)));
        equations.push(Equation::new("vesc", &vec!["r"], Box::new(vesc_r1)));
        equations.push(Equation::new("r", &vec!["vesc"], Box::new(vesc_r2)));

        KeplerianEquations {
            variables,
            equations,
        }
    }

    pub fn from_file(filename: &str) -> Result<KeplerianEquations, Box<dyn Error>> {
        let mut eqs = Self::new();
        let toml_string = fs::read_to_string(filename)?;
        let mut eqs_toml: toml::map::Map<_, _> = toml::from_str(&toml_string)?;
        let eqs_toml = eqs_toml.get("KeplerianVariables")
            .expect("failed to find `KeplerianVariables` section in TOML")
            .as_table()
            .expect("failed to parse as a Table");

        for (k, v) in eqs_toml.iter() {
            if let Some(value) = v.as_float() {
                if k == "nu" || k == "phi" {
                    eqs.define(k, value.to_radians());
                } else {
                    eqs.define(k, value);
                }
            }
        }

        Ok(eqs)
    }

    pub fn define(&mut self, symbol: &str, value: f64) {
        self.variables.set(symbol, Some(value));
        self.update_state();
    }

    pub fn update_state(&mut self) {
        loop {
            let mut new_info = false;
            for equation in &self.equations {
                if equation.solvable(&self.variables) && equation.provides_new_data(&self.variables)
                {
                    let new_value = equation.solve(&self.variables);

                    // Update the symbolic variable
                    self.variables.set(&equation.returns, new_value);
                    if new_value.is_some() {
                        new_info = true;
                    }
                }
            }

            if !new_info {
                break;
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::equations::*;

    #[test]
    fn test_specific_mechanical_energy() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("v", 17.678);
        eqs.define("r", 3.9318e4);
        let value = eqs.variables.get("E").unwrap();
        println!("E: {:?}", value);
        assert!((value - 146.09634).abs() < 0.1);

        let mut eqs = KeplerianEquations::new();
        eqs.define("E", 146.11796);
        eqs.define("v", 17.678);
        let value = eqs.variables.get("r").unwrap();
        println!("r: {:?}", value);
        assert!((value - 3.9318e4).abs() < 0.1);

        let mut eqs = KeplerianEquations::new();
        eqs.define("E", 146.11796);
        eqs.define("r", 3.9318e4);
        let value = eqs.variables.get("v").unwrap();
        println!("v: {:?}", value);
        assert!((value - 17.678).abs() < 0.1);
    }

    #[test]
    fn test_flight_path_angle() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("h", 5.6598e5);
        eqs.define("r", 3.9318e4);
        eqs.define("v", 17.678);
        let value = eqs.variables.get("phi").unwrap();
        println!("phi: {:?} deg.", value.to_degrees());
        assert!((value.to_degrees() - 35.42).abs() < 0.1);

        let mut eqs = KeplerianEquations::new();
        eqs.define("h", 5.6598e5);
        eqs.define("v", 17.678);
        eqs.define("phi", 35.483255_f64.to_radians());
        let value = eqs.variables.get("r").unwrap();
        println!("r: {:?} km", value);
        assert!((value - 3.9318e4).abs() < 0.1);

        let mut eqs = KeplerianEquations::new();
        eqs.define("h", 5.6598e5);
        eqs.define("r", 3.9318e4);
        eqs.define("phi", 35.483255_f64.to_radians());
        let value = eqs.variables.get("v").unwrap();
        println!("v: {:?} km/s", value);
        assert!((value - 17.678).abs() < 0.1);

        let mut eqs = KeplerianEquations::new();
        eqs.define("r", 3.9318e4);
        eqs.define("v", 17.678);
        eqs.define("phi", 35.483255_f64.to_radians());
        let value = eqs.variables.get("h").unwrap();
        println!("h: {:?} km^2/s", value);
        assert!((value - 5.6598e5).abs() < 0.1);
    }

    #[test]
    fn test_example2() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("E", -20.0);
        eqs.define("e", 0.2);
        let a = eqs.variables.get("a").unwrap();
        let p = eqs.variables.get("p").unwrap();
        let h = eqs.variables.get("h").unwrap();
        println!("a: {} km", a);
        println!("p: {} km", p);
        println!("h: {} km^2/s", h);
        assert!((a - 9.96503e3).abs() < 0.1);
        assert!((p - 9.566429e3).abs() < 0.1);
        assert!((h - 6.175103e4).abs() < 0.1);
    }

    #[test]
    fn test_example3() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("e", 0.1);
        eqs.define("rp", 400. + Geocentric.DU);
        let ra = eqs.variables.get("ra").unwrap();
        let E = eqs.variables.get("E").unwrap();
        let h = eqs.variables.get("h").unwrap();
        println!("ra: {} km", ra - Geocentric.DU);
        println!("E: {} km^2/s^2", E);
        println!("h: {} km^2/s", h);
        assert!((ra - 8284.4).abs() < 0.1);
        assert!((E - -26.4631).abs() < 0.1);
        assert!((h - 54515.6).abs() < 0.1);
    }

    #[test]
    fn test_example4() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("e", 0.0);
        eqs.define("r", 200. + Geocentric.DU);

        let vesc = eqs.variables.get("vesc").unwrap();
        println!("vesc: {} km/s", vesc);
        assert!((vesc - 11.0086).abs() < 1e-3);

        let mut eqs = KeplerianEquations::new();
        eqs.define("e", 1.0);
        eqs.define("rp", 200. + Geocentric.DU);
        eqs.define("v", vesc);

        let p = eqs.variables.get("p").unwrap();
        println!("p: {} km", p);
        assert!((p - 13156.28).abs() < 1e-1);
    }

    #[test]
    fn test_example5() {
        let mut eqs = KeplerianEquations::new();
        eqs.define("r", 3189.069 + Geocentric.DU);
        eqs.define("v", 7.90537);
        eqs.define("phi", 0.);

        let E = eqs.variables.get("E").unwrap();
        println!("E: {} km^2/s^2", E);
        assert!((E - -10.417).abs() < 1e-2);

        let h = eqs.variables.get("h").unwrap();
        println!("h: {} km^2/s", h);
        assert!((h - 7.56324e4).abs() < 1.);

        let p = eqs.variables.get("p").unwrap();
        println!("p: {} km", p);
        assert!((p - 1.43508e4).abs() < 1.);

        let e = eqs.variables.get("e").unwrap();
        println!("e: {}", e);
        assert!((e - 0.5).abs() < 1e-3);

        let ra = eqs.variables.get("ra").unwrap();
        println!("ra: {} km", ra);
        assert!((ra - 2.87017e4).abs() < 1.);

        let rp = eqs.variables.get("rp").unwrap();
        println!("rp: {} km", rp);
        assert!((rp - 9.56722e3).abs() < 1.);
    }

    #[test]
    fn test_from_file() {
        let mut eqs = KeplerianEquations::from_file("example.toml").unwrap();
        println!("loaded from file: {:?}", eqs.variables);
        let a = eqs.variables.get("a").unwrap();
        let e = eqs.variables.get("e").unwrap();
        let nu = eqs.variables.get("nu").unwrap().to_degrees();
        let r = eqs.variables.get("r").unwrap();
        let phi = eqs.variables.get("phi").unwrap().to_degrees();
        assert!((a - 8000.0).abs() < 1e-6);
        assert!((e - 0.2).abs() < 1e-6);
        assert!((nu - 32.1).abs() < 1e-6);
        assert!((r - 6567.333556).abs() < 1e-3);
        assert!((phi - 0.09063_f64.to_degrees()).abs() < 1e-1);
    }
}
