mod equations;
mod render;
mod system;

use crate::equations::*;
use crate::render::*;
use eframe::egui;

fn main() {
    let app = TemplateApp::default();
    let mut native_options = eframe::NativeOptions::default();
    native_options.initial_window_size = Some(egui::Vec2::new(400., 600.));
    native_options.resizable = false;
    eframe::run_native(Box::new(app), native_options);
}
