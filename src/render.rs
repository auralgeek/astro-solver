use crate::*;
use eframe::{egui, epi};

#[cfg_attr(feature = "persistence", derive(serde::Deserialize, serde::Serialize))]
#[cfg_attr(feature = "persistence", serde(default))]
pub struct TemplateApp {
    load_filename: String,
    semimajor_axis_string: String,
    eccentricity_string: String,
    specific_mechanical_energy_string: String,
    angular_momentum_string: String,
    flight_path_angle_string: String,
    semi_latus_rectum_string: String,
    range_string: String,
    speed_string: String,
    true_anomaly_string: String,
    periapsis_range_string: String,
    apoapsis_range_string: String,
    periapsis_speed_string: String,
    apoapsis_speed_string: String,
    orbital_period_string: String,
}

impl Default for TemplateApp {
    fn default() -> Self {
        Self {
            load_filename: "example.toml".to_owned(),
            semimajor_axis_string: "None".to_owned(),
            eccentricity_string: "None".to_owned(),
            specific_mechanical_energy_string: "None".to_owned(),
            angular_momentum_string: "None".to_owned(),
            flight_path_angle_string: "None".to_owned(),
            semi_latus_rectum_string: "None".to_owned(),
            range_string: "None".to_owned(),
            speed_string: "None".to_owned(),
            true_anomaly_string: "None".to_owned(),
            periapsis_range_string: "None".to_owned(),
            apoapsis_range_string: "None".to_owned(),
            periapsis_speed_string: "None".to_owned(),
            apoapsis_speed_string: "None".to_owned(),
            orbital_period_string: "None".to_owned(),
        }
    }
}

// Grab the input as a float if we can.
fn parse_input_text_field(field: &str) -> Option<f64> {
    if let Ok(value) = field.parse() {
        Some(value)
    } else {
        None
    }
}

impl epi::App for TemplateApp {
    fn name(&self) -> &str {
        "AstroSolver"
    }

    /// Called once before the first frame.
    fn setup(
        &mut self,
        ctx: &egui::Context,
        _frame: &epi::Frame,
        _storage: Option<&dyn epi::Storage>,
    ) {
        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        ctx.set_visuals(egui::Visuals::dark());
        #[cfg(feature = "persistence")]
        if let Some(storage) = _storage {
            *self = epi::get_value(storage, epi::APP_KEY).unwrap_or_default()
        }
    }

    /// Called by the frame work to save state before shutdown.
    /// Note that you must enable the `persistence` feature for this to work.
    #[cfg(feature = "persistence")]
    fn save(&mut self, storage: &mut dyn epi::Storage) {
        epi::set_value(storage, epi::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, frame: &epi::Frame) {
        let Self {
            load_filename,
            semimajor_axis_string,
            eccentricity_string,
            specific_mechanical_energy_string,
            angular_momentum_string,
            flight_path_angle_string,
            semi_latus_rectum_string,
            range_string,
            speed_string,
            true_anomaly_string,
            periapsis_range_string,
            apoapsis_range_string,
            periapsis_speed_string,
            apoapsis_speed_string,
            orbital_period_string,
        } = self;

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("AstroSolver");

            ui.separator();

            egui::Grid::new("astro_solver_grid")
                .num_columns(2)
                .spacing([40.0, 4.0])
                .striped(true)
                .show(ui, |ui| {
                    ui.text_edit_singleline(load_filename);
                    if ui.add(egui::Button::new("Load File")).clicked() {
                        if let Ok(eqs) = KeplerianEquations::from_file(load_filename) {
                            *semimajor_axis_string = eqs.variables.get("a").unwrap().to_string();
                            *eccentricity_string = eqs.variables.get("e").unwrap().to_string();
                            *specific_mechanical_energy_string = eqs.variables.get("E").unwrap().to_string();
                            *angular_momentum_string = eqs.variables.get("h").unwrap().to_string();
                            *flight_path_angle_string = eqs.variables.get("phi").unwrap().to_string();
                            *semi_latus_rectum_string = eqs.variables.get("p").unwrap().to_string();
                            *range_string = eqs.variables.get("r").unwrap().to_string();
                            *speed_string = eqs.variables.get("v").unwrap().to_string();
                            *true_anomaly_string = eqs.variables.get("nu").unwrap().to_string();
                            *periapsis_range_string = eqs.variables.get("rp").unwrap().to_string();
                            *apoapsis_range_string = eqs.variables.get("ra").unwrap().to_string();
                            *periapsis_speed_string = eqs.variables.get("vp").unwrap().to_string();
                            *apoapsis_speed_string = eqs.variables.get("va").unwrap().to_string();
                            *orbital_period_string = eqs.variables.get("T").unwrap().to_string();
                        } else {
                            println!("File {} not found", load_filename.clone());
                        }
                    }
                    ui.end_row();
                    ui.separator();
                    ui.separator();
                    ui.end_row();
                    ui.label("Parameter Name (unit)");
                    ui.label("Value (float or 'None')");
                    ui.end_row();
                    ui.label("Semimajor Axis (km): ");
                    ui.text_edit_singleline(semimajor_axis_string);
                    ui.end_row();
                    ui.label("Eccentricity: ");
                    ui.text_edit_singleline(eccentricity_string);
                    ui.end_row();
                    ui.label("Specific Mechanical Energy (km^2/s^2): ");
                    ui.text_edit_singleline(specific_mechanical_energy_string);
                    ui.end_row();
                    ui.label("Angular Momentum (km^2/s): ");
                    ui.text_edit_singleline(angular_momentum_string);
                    ui.end_row();
                    ui.label("Flight Path Angle (deg.): ");
                    ui.text_edit_singleline(flight_path_angle_string);
                    ui.end_row();
                    ui.label("Semi Latus Rectum (km): ");
                    ui.text_edit_singleline(semi_latus_rectum_string);
                    ui.end_row();
                    ui.label("SV Range from Origin (km): ");
                    ui.text_edit_singleline(range_string);
                    ui.end_row();
                    ui.label("SV Speed (km/s): ");
                    ui.text_edit_singleline(speed_string);
                    ui.end_row();
                    ui.label("True Anomaly (deg.): ");
                    ui.text_edit_singleline(true_anomaly_string);
                    ui.end_row();
                    ui.label("Periapsis Range (km): ");
                    ui.text_edit_singleline(periapsis_range_string);
                    ui.end_row();
                    ui.label("Apoapsis Range (km): ");
                    ui.text_edit_singleline(apoapsis_range_string);
                    ui.end_row();
                    ui.label("Periapsis Speed (km/s): ");
                    ui.text_edit_singleline(periapsis_speed_string);
                    ui.end_row();
                    ui.label("Apoapsis Speed (km/s): ");
                    ui.text_edit_singleline(apoapsis_speed_string);
                    ui.end_row();
                    ui.label("Orbital Period (s): ");
                    ui.text_edit_singleline(orbital_period_string);
                    ui.end_row();
                });

            if ui.add(egui::Button::new("Solve")).clicked() {
                let a = parse_input_text_field(&semimajor_axis_string);
                println!("Semimajor Axis parsed as: {:?}", a);
                let e = parse_input_text_field(&eccentricity_string);
                println!("Eccentricity parsed as: {:?}", e);
                let E = parse_input_text_field(&specific_mechanical_energy_string);
                println!("Specific Mechanical Energy parsed as: {:?}", E);
                let h = parse_input_text_field(&angular_momentum_string);
                println!("Angular Momentum parsed as: {:?}", h);
                let phi = parse_input_text_field(&flight_path_angle_string);
                println!("Flight Path Angle parsed as: {:?}", phi);
                let p = parse_input_text_field(&semi_latus_rectum_string);
                println!("Semi Latus Rectum parsed as: {:?}", p);
                let r = parse_input_text_field(&range_string);
                println!("SV Range parsed as: {:?}", r);
                let v = parse_input_text_field(&speed_string);
                println!("SV Speed parsed as: {:?}", v);
                let nu = parse_input_text_field(&true_anomaly_string);
                println!("True Anomaly parsed as: {:?}", nu);
                let rp = parse_input_text_field(&periapsis_range_string);
                println!("Periapsis Range parsed as: {:?}", rp);
                let ra = parse_input_text_field(&apoapsis_range_string);
                println!("Apoapsis Range parsed as: {:?}", ra);
                let vp = parse_input_text_field(&periapsis_speed_string);
                println!("Periapsis Speed parsed as: {:?}", vp);
                let va = parse_input_text_field(&apoapsis_speed_string);
                println!("Apoapsis Speed parsed as: {:?}", va);
                let T = parse_input_text_field(&orbital_period_string);
                println!("Orbital Period parsed as: {:?}", T);

                let values = vec![p, e, E, h, r, v, rp, vp, ra, va, a, phi, nu, T];
                let symbols = vec![
                    "p", "e", "E", "h", "r", "v", "rp", "vp", "ra", "va", "a", "phi", "nu", "T",
                ];

                // Dump the known values into a solver
                let mut eqs = KeplerianEquations::new();
                for (v, s) in values.iter().zip(symbols.iter()) {
                    if let Some(value) = v {
                        if *s == "nu" || *s == "phi" {
                            eqs.define(s, value.to_radians());
                        } else {
                            eqs.define(s, *value);
                        }
                    }
                }

                for s in symbols {
                    if let Some(value) = eqs.variables.get(s) {
                        match s {
                            "p" => *semi_latus_rectum_string = value.to_string(),
                            "e" => *eccentricity_string = value.to_string(),
                            "E" => *specific_mechanical_energy_string = value.to_string(),
                            "h" => *angular_momentum_string = value.to_string(),
                            "r" => *range_string = value.to_string(),
                            "v" => *speed_string = value.to_string(),
                            "rp" => *periapsis_range_string = value.to_string(),
                            "vp" => *periapsis_speed_string = value.to_string(),
                            "ra" => *apoapsis_range_string = value.to_string(),
                            "va" => *apoapsis_speed_string = value.to_string(),
                            "a" => *semimajor_axis_string = value.to_string(),
                            "phi" => *flight_path_angle_string = value.to_degrees().to_string(),
                            "nu" => *true_anomaly_string = value.to_degrees().to_string(),
                            "T" => *orbital_period_string = value.to_string(),
                            _ => (),
                        }
                    }
                }
            }

            ui.separator();
            // TODO: Maybe put some orbit plotter here someday
            ui.separator();

            if ui.button("Exit").clicked() {
                frame.quit();
            }
        });
    }
}
