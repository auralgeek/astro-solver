use crate::*;

#[derive(Clone, Debug, PartialEq)]
pub struct Variables {
    pub variables: Vec<Variable>,
}

impl Variables {
    pub fn new() -> Variables {
        Variables { variables: vec![] }
    }

    pub fn get(&self, symbol: &str) -> Option<f64> {
        for var in &self.variables {
            if var.symbol == symbol {
                return var.value;
            }
        }
        None
    }

    pub fn set(&mut self, symbol: &str, value: Option<f64>) {
        for var in &mut self.variables {
            if var.symbol == symbol {
                var.value = value;
                return;
            }
        }

        // If we get here, then this variable doesn't already exist, so create
        // it.
        self.variables.push(Variable::new(symbol, value));
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Variable {
    pub symbol: String,
    pub value: Option<f64>,
}

impl Variable {
    pub fn new(symbol: &str, value: Option<f64>) -> Variable {
        Variable {
            symbol: symbol.to_string(),
            value,
        }
    }
}

// Functions need signatures for logic in solving new symbolic variables
pub struct Equation {
    pub returns: String,
    pub parameters: Vec<String>,
    pub function: Box<dyn Fn(&Variables) -> Option<f64>>,
}

impl Equation {
    pub fn new(
        returns: &str,
        parameters: &[&str],
        function: Box<dyn Fn(&Variables) -> Option<f64>>,
    ) -> Equation {
        let mut owned_params = vec![];
        for param in parameters {
            owned_params.push(param.to_string());
        }
        Equation {
            returns: returns.to_string(),
            parameters: owned_params,
            function,
        }
    }

    pub fn solvable(&self, variables: &Variables) -> bool {
        for param in &self.parameters {
            if variables.get(param).is_none() {
                // Bail immediately if any required parameter isn't yet
                // available.
                return false;
            }
        }

        // If we got here, then all the parameters required are ready.
        true
    }

    pub fn provides_new_data(&self, variables: &Variables) -> bool {
        let var = variables.get(&self.returns);
        if var.is_none() {
            return true;
        } else {
            return false;
        }
    }

    pub fn solve(&self, variables: &Variables) -> Option<f64> {
        (self.function)(variables)
    }
}
